/* Quatter
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "board.h"
#include "piece.h"
#include "ui/indicator.h"
#include "effectmaster.h"

void Board::RegisterObject(Context *context)
{
    context->RegisterFactory<Board>();
}

Board::Board(Context* context): LogicComponent(context),
    squares_{},
    selectedSquare_{},
    lastSelectedSquare_{},
    indicators_{}
{
    SetUpdateEventMask(USE_UPDATE);
    SubscribeToEvent(E_GRAPHICSQUALITYCHANGED, DRY_HANDLER(Board, HandleGraphicsQualityChanged));
}

void Board::Start()
{
    node_->AddTag("Board");
    model_ = node_->CreateComponent<StaticModel>();
    model_->SetCastShadows(true);

    UpdateGraphics();
    CreateSquares();
    CreateIndicators();
}

void Board::UpdateGraphics()
{
    const String postfix{ MC->GetGraphicsQuality() == QUALITY_LOW ? "_LOQ" : "" };

    model_->SetModel(RES_MDL("Board" + postfix));
    model_->SetMaterial(RES_MAT("Board"));
}

void Board::CreateSquares()
{
    for (int i{ 0 }; i < WIDTH; ++i)
    {
        for (int j{ 0 }; j < HEIGHT; ++j)
        {
            Node* squareNode{ node_->CreateChild("Square", LOCAL) };
            Square* square{ squareNode->CreateComponent<Square>() };
            IntVector2 coords{ i, j };

            square->coords_ = coords;
            squareNode->SetPosition(CoordsToPosition(coords));
            squares_[square->coords_] = square;
        }
    }
}

void Board::CreateIndicators()
{
    for (int i{ 0 }; i < 6; ++i)
    {
        Node* indicatorNode{ node_->CreateChild("Indicator", LOCAL) };
        indicatorNode->SetPosition(GetThickness() * Vector3::UP);
        Indicator* indicator{ indicatorNode->CreateComponent<Indicator>() };
        indicator->Init(i);

        indicators_.Push(indicator);
    }
}

void Board::Reset()
{
    for (Square* s: squares_.Values())
    {
        s->free_ = true;
        s->piece_ = nullptr;

        if (s->light_)
            s->light_->SetEnabled(true);
    }

    Deselect();
}

void Board::Refuse()
{
    if (selectedSquare_)
    {
        Material* glow{ selectedSquare_->slot_->GetMaterial() };
        glow->SetShaderParameter("MatDiffColor", Color{ 1.f, .0f, .0f, 1.f });

        if (selectedSquare_->free_)
            FX->FadeTo(glow, COLOR_GLOW, .23f);
        else
            FX->FadeTo(glow, Color{ 1.f, .8f, .0f, .5f }, .23f);
    }
}

Composition Board::GetComposition() const
{
    Composition composition{};
    for (int x{ 0 }; x < 4; ++x)
    for (int y{ 0 }; y < 4; ++y)
    {
        const IntVector2 coords{ x, y };
        Square* s{ *squares_[coords] };
        Piece*  p{ s->piece_ };

        composition[{ x, y }] = (p ? p->ToInt() : -1);
    }

    return composition;
}

bool Board::IsEmpty() const
{
    for (Square* s : squares_.Values())
    {
        if (!s->free_)
            return false;
    }

    return true;
}

bool Board::IsFull() const
{
    for (Square* s : squares_.Values())
    {
        if (s->free_)
            return false;
    }

    return true;
}

Vector3 Board::CoordsToPosition(IntVector2 coords)
{
    return Vector3{ .5f + coords.x_ - WIDTH / 2,
                    GetThickness(),
                    .5f + coords.y_ - HEIGHT / 2 };
}

void Board::Update(float /*timeStep*/)
{
    for (Square* s: squares_.Values())
        s->slot_->SetMorphWeight(0, MC->Sine(2.3f, .0f, 1.f));
}

bool Board::PutPiece(Piece* piece, Square* square)
{
    if (!square)
        return PutPiece(piece);

    if (piece && square->free_)
    {
        MC->DeselectPiece();

        square->piece_ = piece;
        square->free_ = false;

        if (square->light_)
            square->light_->SetEnabled(false);

        piece->Put(square->GetNode()->GetWorldPosition()
                   + Vector3{ Random(-.05f, .05f),
                              .0f,
                              Random(-.05f, .05f) });
        Deselect();
        lastSelectedSquare_ = nullptr;

        if (CheckQuatter())
            MC->Quatter();

        MC->NextPhase();
        return true;
    }
    else
    {
        Refuse();
        return false;
    }
}

bool Board::PutPiece(Piece* piece)
{
    if (!selectedSquare_)
    {
        SelectLast();
        return false;
    }
    else
    {
        return PutPiece(piece, selectedSquare_);
    }
}

bool Board::PutPiece(Square* square)
{
    return PutPiece(MC->GetPickedPiece(), square);
}

bool Board::PutPiece()
{
    return PutPiece(MC->GetPickedPiece());
}

Square* Board::GetNearestSquare(const Vector3& pos, bool free)
{
    Square* nearest{ nullptr };

    for (Square* s: squares_.Values())
    {
        if (!nearest
         || s->node_->GetWorldPosition().DistanceToPoint(pos) <
            nearest->node_->GetWorldPosition().DistanceToPoint(pos))
        {
            if (s->free_ || !free)
                nearest = s;
        }
    }

    return nearest;
}

void Board::SelectNearestFreeSquare(const Vector3& pos)
{
    Square* square{GetNearestSquare(pos, true)};

    if (square)
        Select(square);
}

void Board::SelectNearestSquare(const Vector3& pos)
{
    Square* square{ GetNearestSquare(pos, false) };

    if (square)
        Select(square);
}

void Board::SelectLast()
{
    if (lastSelectedSquare_ && lastSelectedSquare_ != selectedSquare_)
        Select(lastSelectedSquare_);
    else if (!selectedSquare_)
        SelectNearestFreeSquare(CAMERA->GetPosition());
}

void Board::Select(Square* square)
{
    Deselect();

    selectedSquare_ = square;
    square->selected_ = true;

    //Fade in slot and light
    if (square->free_)
    {
        FX->FadeTo(square->slot_->GetMaterial(),
                   COLOR_GLOW);
    }
    else
    {
        FX->FadeTo(square->slot_->GetMaterial(),
                   Color{ 1.f, .8f, .0f, .5f });
    }

    if (square->light_)
        FX->FadeTo(square->light_, .42f);

    if (!MC->IsAITurn())
        Indicate(square->coords_);
}

void Board::Deselect()
{
    HideIndicators();

    if (!selectedSquare_)
        return;

    //Fade out slot and light
    FX->FadeOut(selectedSquare_->slot_->GetMaterial());

    if (selectedSquare_->light_)
        FX->FadeTo(selectedSquare_->light_, .023f);

    lastSelectedSquare_ = selectedSquare_;
    selectedSquare_->selected_ = false;
    selectedSquare_ = nullptr;
}

void Board::Step(const IntVector2& step)
{
    if (selectedSquare_)
    {
        IntVector2 newCoords{ selectedSquare_->coords_ + step };

        if (squares_.Contains(newCoords))
            Select(squares_[newCoords]);
    }
    else
    {
        SelectLast();
    }
}

bool Board::CheckQuatter()
{
    bool checkRows{ true };
    bool checkBlocks{ true };

    return (checkRows && CheckRows()) || (checkBlocks && CheckBlocks());
}

bool Board::CheckRows()
{
    for (int j{ 0 }; j < HEIGHT; ++j)
    {
        Piece::PieceAttributes matching{};
        matching.flip();
        Piece::PieceAttributes first{};

        for (int i{ 0 }; i < WIDTH; ++i)
        {
            IntVector2 coords(i, j);
            Piece* piece{squares_[coords]->piece_};

            if (piece)
            {
                Piece::PieceAttributes attributes{ piece->GetPieceAttributes() };

                if (i == 0)
                {
                    first = attributes;

                }
                else
                {
                    for (int a{ 0 }; a < Piece::NUM_ATTRIBUTES; ++a)
                    {
                        if (first[a] != attributes[a])
                            matching[a] = false;
                    }
                }
                //Full row required
            }
            else
            {
                matching.reset();
                break;
            }
        }
        //Quatter!
        if (matching.any())
        {
            Indicate(IntVector2{ 0, j },
                     IntVector2{ WIDTH - 1, j });

            return true;
        }
    }

    //Check columns
    for (int i{ 0 }; i < WIDTH; ++i)
    {
        Piece::PieceAttributes matching{};
        matching.flip();
        Piece::PieceAttributes first{};

        for (int j{ 0 }; j < HEIGHT; ++j)
        {
            IntVector2 coords{ i, j };
            Piece* piece{ squares_[coords]->piece_ };

            if (piece)
            {
                Piece::PieceAttributes attributes{ piece->GetPieceAttributes() };

                if (j == 0)
                {
                    first = attributes;
                }
                else
                {
                    for (int a{ 0 }; a < Piece::NUM_ATTRIBUTES; ++a)
                        if (first[a] != attributes[a])
                            matching[a] = false;
                }
                //Full column required
            }
            else
            {
                matching.reset();
                break;
            }
        }
        //Quatter!
        if (matching.any())
        {
            Indicate(IntVector2{ i, 0 },
                     IntVector2{ i, HEIGHT - 1 });
            return true;
        }
    }

    //Check diagonals
    for (bool direction : { true, false })
    {
        Piece::PieceAttributes matching{};
        matching.flip();
        Piece::PieceAttributes first{};

        for (int i{ 0 }; i < WIDTH; ++i)
        {
            IntVector2 coords{ i, direction ? i : (WIDTH - i - 1) };
            Piece* piece{ squares_[coords]->piece_ };

            if (piece)
            {
                Piece::PieceAttributes attributes{ piece->GetPieceAttributes() };

                if (i == 0)
                {
                    first = attributes;
                }
                else
                {
                    for (int a{ 0 }; a < Piece::NUM_ATTRIBUTES; ++a)
                    {
                        if (first[a] != attributes[a])
                            matching[a] = false;
                    }
                }
                //Full line required
            }
            else
            {
                matching.reset();
                break;
            }
        }
        //Quatter!
        if (matching.any())
        {
            Indicate(IntVector2{ 0,                direction * (HEIGHT - 1) },
                     IntVector2{ WIDTH - 1, !direction * (HEIGHT - 1) });

            return true;
        }
    }

    return false;
}

bool Board::CheckBlocks()
{
    for (int k{ 0 }; k < WIDTH - 1; ++k)
    {
        for (int l{ 0 }; l < HEIGHT - 1; ++l)
        {

            Piece::PieceAttributes matching{};
            matching.flip();
            Piece::PieceAttributes first{};

            for (int m: { 0, 1 })
            {
                for (int n : { 0, 1 })
                {
                    IntVector2 coords{ k + m, l + n };
                    Piece* piece{ squares_[coords]->piece_ };

                    if (piece)
                    {
                        Piece::PieceAttributes attributes{ piece->GetPieceAttributes() };

                        if (m == 0 && n == 0)
                        {
                            first = attributes;
                        }
                        else
                        {
                            for (int a{ 0 }; a < Piece::NUM_ATTRIBUTES; ++a)
                            {
                                if (first[a] != attributes[a])
                                    matching[a] = false;
                            }
                        }
                        //Full block required
                    }
                    else
                    {
                        matching.reset();
                        break;
                    }
                }
            }
            //Quatter!
            if (matching.any())
            {
                Indicate(IntVector2{ k, l },
                         IntVector2{ k + 1, l + 1 });
                return true;
            }
        }
    }

    return false;
}

void Board::Indicate(const IntVector2& first, const IntVector2& last)
{
    //Indicate single square (for keyboard selection)
    if (last == IntVector2(-1, -1))
    {
        FadeInIndicator(indicators_[0], true);
        FX->TransformTo(indicators_[0]->GetNode(),
                CoordsToPosition(first) * Vector3{ 0.0f, 1.0f, 1.0f },
                indicators_[0]->GetNode()->GetRotation(),
                0.05f);
        indicators_[0]->model1_->SetMorphWeight(1, static_cast<float>(first.y_ > 0 && first.y_ < 3));
        indicators_[0]->model2_->SetMorphWeight(1, static_cast<float>(first.y_ > 0 && first.y_ < 3));

        FadeInIndicator(indicators_[1], true);
        FX->TransformTo(indicators_[1]->GetNode(),
                CoordsToPosition(first) * Vector3(1.0f, 1.0f, 0.0f),
                indicators_[1]->GetNode()->GetRotation(),
                0.05f);
        indicators_[1]->model1_->SetMorphWeight(1, static_cast<float>(first.x_ > 0 && first.x_ < 3));
        indicators_[1]->model2_->SetMorphWeight(1, static_cast<float>(first.x_ > 0 && first.x_ < 3));

    //Indicate row
    }
    else if (first.y_ == last.y_)
    {
        FadeInIndicator(indicators_[0]);
        indicators_[0]->GetNode()->SetPosition(CoordsToPosition(first) * Vector3{ 0.f, 1.f, 1.f });
        indicators_[0]->model1_->SetMorphWeight(1, static_cast<float>(first.y_ > 0 && first.y_ < 3));
        indicators_[0]->model2_->SetMorphWeight(1, static_cast<float>(first.y_ > 0 && first.y_ < 3));

    //Indicate column
    }
    else if (first.x_ == last.x_)
    {
        FadeInIndicator(indicators_[1]);
        indicators_[1]->GetNode()->SetPosition(CoordsToPosition(first) * Vector3{ 1.f, 1.f, 0.f });
        indicators_[1]->model1_->SetMorphWeight(1, static_cast<float>(first.x_ > 0 && first.x_ < 3));
        indicators_[1]->model2_->SetMorphWeight(1, static_cast<float>(first.x_ > 0 && first.x_ < 3));

    //Indicate first diagonal
    }
    else if (first.x_ == 0 && last.y_ == 0)
    {
        FadeInIndicator(indicators_[3]);

    //Indicate 2x2 blocks
    }
    else if (last.x_ - first.x_ == 1)
    {
        FadeInIndicator(indicators_[4]);
        indicators_[4]->GetNode()->SetPosition(CoordsToPosition(first) * Vector3{ 0.f, 1.f, 1.f });
        FadeInIndicator(indicators_[5]);
        indicators_[5]->GetNode()->SetPosition(CoordsToPosition(first) * Vector3{ 1.f, 1.f, 0.f });

    //Indicate other diagonal
    }
    else
    {
        FadeInIndicator(indicators_[2]);
    }
}

void Board::FadeInIndicator(Indicator* indicator, bool fast)
{
    FX->FadeTo(indicator->glow_, COLOR_GLOW, fast ? .23f : 2.3f, fast ? .0f : 1.f);
}

void Board::HideIndicators()
{
    for (Indicator* i: indicators_)
        FX->FadeOut(i->glow_);
}
