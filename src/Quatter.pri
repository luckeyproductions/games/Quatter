SOURCES += \
    $$PWD/ai.cpp \
    $$PWD/luckey.cpp \
    $$PWD/mastercontrol.cpp \
    $$PWD/inputmaster.cpp \
    $$PWD/quattercam.cpp \
    $$PWD/board.cpp \
    $$PWD/piece.cpp \
    $$PWD/master.cpp \
    $$PWD/effectmaster.cpp \
    $$PWD/square.cpp \
    $$PWD/ui/yad.cpp \
    $$PWD/ui/indicator.cpp \
    $$PWD/ui/guimaster.cpp

HEADERS += \
    $$PWD/ai.h \
    $$PWD/luckey.h \
    $$PWD/mastercontrol.h \
    $$PWD/inputmaster.h \
    $$PWD/quattercam.h \
    $$PWD/board.h \
    $$PWD/piece.h \
    $$PWD/master.h \
    $$PWD/effectmaster.h \
    $$PWD/square.h \
    $$PWD/ui/yad.h \
    $$PWD/ui/indicator.h \
    $$PWD/ui/guimaster.h
